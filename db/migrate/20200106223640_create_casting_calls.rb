class CreateCastingCalls < ActiveRecord::Migration[6.0]
  def change
    create_table :casting_calls do |t|
      t.string :name
      t.text :info
      t.integer :post_id
      t.string :system
      t.string :video
      t.string :audio
      t.string :studio
      t.string :live
      t.string :pitch
      t.integer :seats
      t.string :paid
      t.string :paid_info
      t.string :platform

      t.timestamps
    end
  end
end
