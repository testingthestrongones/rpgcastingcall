class CreateApplications < ActiveRecord::Migration[6.0]
  def change
    create_table :applications do |t|
      t.string :player
      t.text :body
      t.string :url

      t.timestamps
    end
  end
end
