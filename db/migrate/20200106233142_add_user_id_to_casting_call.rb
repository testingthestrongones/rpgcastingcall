class AddUserIdToCastingCall < ActiveRecord::Migration[6.0]
  def change
    add_column :casting_calls, :user_id, :integer
  end
end
