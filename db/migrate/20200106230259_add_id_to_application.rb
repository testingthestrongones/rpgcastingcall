class AddIdToApplication < ActiveRecord::Migration[6.0]
  def change
    add_column :applications, :post_id, :integer
  end
end
