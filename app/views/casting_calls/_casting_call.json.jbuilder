json.extract! casting_call, :id, :name, :info, :post_id, :system, :video, :audio, :studio, :live, :pitch, :seats, :paid, :paid_info, :platform, :created_at, :updated_at
json.url casting_call_url(casting_call, format: :json)
