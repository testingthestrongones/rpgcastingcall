json.extract! application, :id, :player, :body, :url, :created_at, :updated_at
json.url application_url(application, format: :json)
