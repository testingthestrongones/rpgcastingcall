class CastingCallsController < ApplicationController
  before_action :set_casting_call, only: [:show, :edit, :update, :destroy]

  # GET /casting_calls
  # GET /casting_calls.json
  def index
    @casting_calls = CastingCall.all
  end

  # GET /casting_calls/1
  # GET /casting_calls/1.json
  def show
    @application = Application.new
  end

  # GET /casting_calls/new
  def new    
    @casting_call = CastingCall.new
  end

  # GET /casting_calls/1/edit
  def edit
  end

  # POST /casting_calls
  # POST /casting_calls.json
  def create
    @casting_call = CastingCall.new(casting_call_params)

    respond_to do |format|
      if @casting_call.save
        format.html { redirect_to @casting_call, notice: 'Casting call was successfully created.' }
        format.json { render :show, status: :created, location: @casting_call }
      else
        format.html { render :new }
        format.json { render json: @casting_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /casting_calls/1
  # PATCH/PUT /casting_calls/1.json
  def update
    respond_to do |format|
      if @casting_call.update(casting_call_params)
        format.html { redirect_to @casting_call, notice: 'Casting call was successfully updated.' }
        format.json { render :show, status: :ok, location: @casting_call }
      else
        format.html { render :edit }
        format.json { render json: @casting_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /casting_calls/1
  # DELETE /casting_calls/1.json
  def destroy
    @casting_call.destroy
    respond_to do |format|
      format.html { redirect_to casting_calls_url, notice: 'Casting call was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_casting_call
      @casting_call = CastingCall.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def casting_call_params
      params.require(:casting_call).permit(:name, :info, :post_id, :system, :video, :audio, :studio, :live, :pitch, :seats, :paid, :paid_info, :platform, :user_id)
    end
end
