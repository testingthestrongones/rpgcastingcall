require 'test_helper'

class CastingCallsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @casting_call = casting_calls(:one)
  end

  test "should get index" do
    get casting_calls_url
    assert_response :success
  end

  test "should get new" do
    get new_casting_call_url
    assert_response :success
  end

  test "should create casting_call" do
    assert_difference('CastingCall.count') do
      post casting_calls_url, params: { casting_call: { audio: @casting_call.audio, info: @casting_call.info, live: @casting_call.live, name: @casting_call.name, paid: @casting_call.paid, paid_info: @casting_call.paid_info, pitch: @casting_call.pitch, platform: @casting_call.platform, post_id: @casting_call.post_id, seats: @casting_call.seats, studio: @casting_call.studio, system: @casting_call.system, video: @casting_call.video } }
    end

    assert_redirected_to casting_call_url(CastingCall.last)
  end

  test "should show casting_call" do
    get casting_call_url(@casting_call)
    assert_response :success
  end

  test "should get edit" do
    get edit_casting_call_url(@casting_call)
    assert_response :success
  end

  test "should update casting_call" do
    patch casting_call_url(@casting_call), params: { casting_call: { audio: @casting_call.audio, info: @casting_call.info, live: @casting_call.live, name: @casting_call.name, paid: @casting_call.paid, paid_info: @casting_call.paid_info, pitch: @casting_call.pitch, platform: @casting_call.platform, post_id: @casting_call.post_id, seats: @casting_call.seats, studio: @casting_call.studio, system: @casting_call.system, video: @casting_call.video } }
    assert_redirected_to casting_call_url(@casting_call)
  end

  test "should destroy casting_call" do
    assert_difference('CastingCall.count', -1) do
      delete casting_call_url(@casting_call)
    end

    assert_redirected_to casting_calls_url
  end
end
