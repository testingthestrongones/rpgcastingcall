require "application_system_test_case"

class CastingCallsTest < ApplicationSystemTestCase
  setup do
    @casting_call = casting_calls(:one)
  end

  test "visiting the index" do
    visit casting_calls_url
    assert_selector "h1", text: "Casting Calls"
  end

  test "creating a Casting call" do
    visit casting_calls_url
    click_on "New Casting Call"

    fill_in "Audio", with: @casting_call.audio
    fill_in "Info", with: @casting_call.info
    fill_in "Live", with: @casting_call.live
    fill_in "Name", with: @casting_call.name
    fill_in "Paid", with: @casting_call.paid
    fill_in "Paid info", with: @casting_call.paid_info
    fill_in "Pitch", with: @casting_call.pitch
    fill_in "Platform", with: @casting_call.platform
    fill_in "Post", with: @casting_call.post_id
    fill_in "Seats", with: @casting_call.seats
    fill_in "Studio", with: @casting_call.studio
    fill_in "System", with: @casting_call.system
    fill_in "Video", with: @casting_call.video
    click_on "Create Casting call"

    assert_text "Casting call was successfully created"
    click_on "Back"
  end

  test "updating a Casting call" do
    visit casting_calls_url
    click_on "Edit", match: :first

    fill_in "Audio", with: @casting_call.audio
    fill_in "Info", with: @casting_call.info
    fill_in "Live", with: @casting_call.live
    fill_in "Name", with: @casting_call.name
    fill_in "Paid", with: @casting_call.paid
    fill_in "Paid info", with: @casting_call.paid_info
    fill_in "Pitch", with: @casting_call.pitch
    fill_in "Platform", with: @casting_call.platform
    fill_in "Post", with: @casting_call.post_id
    fill_in "Seats", with: @casting_call.seats
    fill_in "Studio", with: @casting_call.studio
    fill_in "System", with: @casting_call.system
    fill_in "Video", with: @casting_call.video
    click_on "Update Casting call"

    assert_text "Casting call was successfully updated"
    click_on "Back"
  end

  test "destroying a Casting call" do
    visit casting_calls_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Casting call was successfully destroyed"
  end
end
