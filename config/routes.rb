Rails.application.routes.draw do
  devise_for :users
  resources :applications
  resources :casting_calls
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'casting_calls#index'
end